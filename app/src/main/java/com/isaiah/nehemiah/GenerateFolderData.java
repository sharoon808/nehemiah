package com.isaiah.nehemiah;

import android.os.Environment;
import android.util.Log;

import com.isaiah.nehemiah.entity.Folder;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 * Created by sharoon on 2019-01-24.
 */

public class GenerateFolderData {

    private ArrayList<Folder> folderList = new ArrayList<Folder>();
    final String ourFolder = "Nehemiah" ;

    public GenerateFolderData() throws FileNotFoundException{
        generateData() ;
    }

    public void generateData() throws FileNotFoundException {

        File directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), ourFolder);
        Log.d("Files", "Size: "+ directory.getAbsolutePath() );
        File[] files = directory.listFiles();
        Log.d("Files", "Size: "+ files.length);
        for (int i = 0; i < files.length; i++){
            File singleFile = files[i];
            long lastModified = singleFile.lastModified();
            Folder sFolder = new Folder( singleFile.getName() , directory.getAbsolutePath() ) ;
            folderList.add(sFolder);
        }
    }

    public ArrayList<Folder> getFolders(){
        return this.folderList ;
    }
}
