package com.isaiah.nehemiah;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View ;

import com.isaiah.nehemiah.entity.Folder;
import com.isaiah.nehemiah.utills.FloderRecyclerViewAdapter;

import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 * Created by sharoon on 2019-01-24.
 */

public class home extends AppCompatActivity {

    private RecyclerView folderListView ;
    private FloatingActionButton cameraActicityAction ;
    private ArrayList<Folder> folderList = new ArrayList<Folder>();
    private Intent cameraIntent ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getPermissions();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initializeElements() ;
        initializeFolderList() ;
        initializeFolderListView() ;
    }

    protected void initializeElements(){
        this.cameraActicityAction = findViewById( R.id.camera_activity ) ;
        this.folderListView = findViewById(R.id.folder_list_view) ;

        this.cameraIntent = new Intent(this , CameraActivity.class);
        this.cameraActicityAction.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                startActivity(cameraIntent);
            }
        });

    }

    protected  void initializeFolderList() {
        GenerateFolderData gfd = null;

        try {
            gfd = new GenerateFolderData();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return ;
        }

        folderList = gfd.getFolders() ;
    }

    protected void initializeFolderListView(){
        this.folderListView.setHasFixedSize(true);
        //this.folderListView.setItemViewCacheSize(10);
        //this.folderListView.setDrawingCacheEnabled(true);
        //this.folderListView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_LOW);

       //RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
       RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 2);
        this.folderListView.setLayoutManager(layoutManager);

        FloderRecyclerViewAdapter adapter = new FloderRecyclerViewAdapter(getApplicationContext() , this.folderList);
        this.folderListView.setAdapter(adapter);
    }

    private void getPermissions(){
        if(checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            //Requesting permission.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }
    }

}
