package com.isaiah.nehemiah;


import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.TextureView;
import android.view.View;

import com.isaiah.nehemiah.utills.CameraController;

/**
 * Created by sharoon on 2019-01-24.
 */

public class CameraActivity extends Activity {
    CameraController cameraController ;
    TextureView textureView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        textureView = (TextureView)findViewById(R.id.textureview);
        //getPermissions();
        try {
            cameraController = new CameraController(CameraActivity.this, textureView);

            findViewById(R.id.btn_takepicture).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cameraController.takePicture();
                }
            });
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /*
    private void getPermissions(){

        if(checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            //Requesting permission.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }
    } */

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                }
                return;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}











