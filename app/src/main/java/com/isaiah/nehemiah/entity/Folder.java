package com.isaiah.nehemiah.entity;

/**
 * Created by sharoon on 2019-01-24.
 */


public class Folder {
    String folderName ;
    String folderImagePath ;
    String createdDate ;

    public Folder(String folderName , String folderImagePath){
        this.folderName = folderName ;
        this.folderImagePath = folderImagePath ;
    }
    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public String getFolderImagePath() {
        return folderImagePath;
    }

    public void setFolderImagePath(String folderImagePath) {
        this.folderImagePath = folderImagePath;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
}
