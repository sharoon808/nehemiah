package com.isaiah.nehemiah.utills;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Parcel;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.isaiah.nehemiah.R;
import com.isaiah.nehemiah.entity.Folder;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 * Created by sharoon on 2019-01-24.
 */

public class FloderRecyclerViewAdapter extends RecyclerView.Adapter<FloderRecyclerViewAdapter.ViewHolder>{

    private ArrayList<Folder> folderList;
    Context context ;
    public FloderRecyclerViewAdapter(Context context , ArrayList<Folder> folderList ){
        this.folderList = folderList ;
        this.context = context ;
    }

    @NonNull
    @Override
    public FloderRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View folderView ;
        folderView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.folder_view, viewGroup, false);
        return new ViewHolder(folderView);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position ) {
        Folder sFolder = folderList.get(position);

        viewHolder.folderName.setText(sFolder.getFolderName());
        Log.d("folder path ", "path: "+ sFolder.getFolderImagePath());
        File file = new File( sFolder.getFolderImagePath() + File.separator + sFolder.getFolderName()) ;
        try {

           FileInputStream fileInputStream = new FileInputStream(file);

           /*  BitmapFactory.Options bounds = new BitmapFactory.Options();
            bounds.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(sFolder.getFolderImagePath() + File.separator + sFolder.getFolderName(), bounds);

            BitmapFactory.Options opts = new BitmapFactory.Options();
            Bitmap bm = BitmapFactory.decodeFile(sFolder.getFolderImagePath() + File.separator + sFolder.getFolderName(), opts);
            ExifInterface exif = null ;
            try{
                exif = new ExifInterface(sFolder.getFolderImagePath() + File.separator + sFolder.getFolderName());
            }catch(Exception e){
                e.printStackTrace();
            }
            String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
            int orientation = orientString != null ? Integer.parseInt(orientString) :  ExifInterface.ORIENTATION_NORMAL;

            int rotationAngle = 0;
            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotationAngle = 90;
            if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotationAngle = 180;
            if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotationAngle = 270;

            Matrix matrix = new Matrix();
            matrix.setRotate(rotationAngle, (float) bm.getWidth() / 2, (float) bm.getHeight() / 2);
            Bitmap rotatedBitmap = Bitmap.createBitmap(bm, 0, 0, bounds.outWidth, bounds.outHeight, matrix, true); */


            Uri uri = Uri.fromFile(file);

            Picasso.with(this.context).load(uri)
                    .resize(96, 96).centerCrop().into(viewHolder.folderImage);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return folderList.size() ;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView folderImage;
        TextView folderName ;
        ViewHolder(View listItemView) {
            super(listItemView);
            folderImage = listItemView.findViewById(R.id.folder_latest_img);
            folderName = listItemView.findViewById(R.id.folder_name) ;
        }
    }
}
