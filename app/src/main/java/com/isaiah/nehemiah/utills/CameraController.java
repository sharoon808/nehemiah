package com.isaiah.nehemiah.utills;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import static android.content.ContentValues.TAG;

/**
 * Created by sharoon on 2019-01-24.
 */

public class CameraController {

    private static final String TAG = "CameraController";

    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();
    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    Activity activity;
    private TextureView textureView ;

    private String avilableCameraId ;
    private CameraDevice cDevice ;
    private CameraCaptureSession cCaptureSession ;

    private Boolean isFlashAvilable ;
    private Size previewSize;

    private CaptureRequest.Builder previewRequestBuilder ;
    private CaptureRequest previewRequest ;

    private HandlerThread cBackgroundThread;
    private Handler cBackgroundHandler ;
    private ImageReader imageReader;
    private File file;

    public CameraController(Activity activity, TextureView textureView){
        this.activity = activity;
        this.textureView = textureView;
        this.textureView.setSurfaceTextureListener(previewSurfaceTextureListener);
    }

    private final TextureView.SurfaceTextureListener previewSurfaceTextureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            openCamera(width,height);
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {

        }
    };
    private final CameraDevice.StateCallback cStateCallback = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(@NonNull CameraDevice cameraDevice){
            cDevice = cameraDevice ;
            createCameraPreviewSession();
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice cameraDevice){
            cameraDevice.close();
            cDevice = null ;
        }

        @Override
        public void onError(@NonNull CameraDevice cameraDevice, int error) {
            cameraDevice.close();
            stopBackgroundThread();
            cDevice = null ;
            if(activity != null ){
                activity.finish();
            }
        }
    };

    private CameraCaptureSession.CaptureCallback cCaptureCallback = new CameraCaptureSession.CaptureCallback() {
        @Override
        public void onCaptureProgressed(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull CaptureResult partialResult) {
            super.onCaptureProgressed(session, request, partialResult);
        }

        @Override
        public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {

        }
    };

    private final ImageReader.OnImageAvailableListener mOnImageAvailableListener = new ImageReader.OnImageAvailableListener() {

        @Override
        public void onImageAvailable(ImageReader reader) {
            Log.d(TAG, "ImageAvailable");
            cBackgroundHandler.post(new ImageSaver(reader.acquireNextImage(), file));
        }

    };

    public void openCamera( int width , int height ){
        if( ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED){
            return;
        }
        startBackgroundThread();
        setUpCameraOutputs(width, height);
        CameraManager cManager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
        try{
            cManager.openCamera(avilableCameraId, cStateCallback , cBackgroundHandler);
        }catch(CameraAccessException cae){
            cae.printStackTrace();
        }
    }

    private void setUpCameraOutputs(int width , int height){
        CameraManager cManager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
        try{
            for(String cameraId : cManager.getCameraIdList()){
                CameraCharacteristics cCharacteristics = cManager.getCameraCharacteristics(cameraId);
                Integer facing = cCharacteristics.get(CameraCharacteristics.LENS_FACING);
                if(facing != null && facing == CameraCharacteristics.LENS_FACING_FRONT){
                    return ;
                }

                StreamConfigurationMap streamConfigMap = cCharacteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                if (streamConfigMap == null) {
                    continue;
                }

                previewSize  = new Size(width , height);

                //Size imageReaderSize = Collections.max(Arrays.asList(streamConfigMap.getOutputSizes(ImageFormat.JPEG)), new CompareSizesByArea());
                Size[] imageReaderSize = streamConfigMap.getOutputSizes(ImageFormat.JPEG);
                imageReader = ImageReader.newInstance(imageReaderSize[0].getWidth() , imageReaderSize[0].getHeight() , ImageFormat.JPEG , 1 );
                imageReader.setOnImageAvailableListener(mOnImageAvailableListener , cBackgroundHandler );

                Boolean available = cCharacteristics.get(CameraCharacteristics.FLASH_INFO_AVAILABLE);
                Log.d(TAG, "Flash avilable : " + available );
                isFlashAvilable = available == null ? false : available;

                avilableCameraId = cameraId;
                return;
            }
        }catch(CameraAccessException cae){
            cae.printStackTrace();
        }catch(NullPointerException e){
            e.printStackTrace();
        }
    }

    private void createCameraPreviewSession(){
        try{
            SurfaceTexture texture = textureView.getSurfaceTexture();
            assert texture != null ;

            texture.setDefaultBufferSize(previewSize.getWidth() ,previewSize.getHeight());

            Surface previewSurface = new Surface(texture);

            previewRequestBuilder = cDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            previewRequestBuilder.addTarget(previewSurface);

            int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
            previewRequestBuilder.set(CaptureRequest.JPEG_ORIENTATION, ORIENTATIONS.get(rotation));

            Surface imageReaderSurface = imageReader.getSurface() ;
            cDevice.createCaptureSession(Arrays.asList(previewSurface , imageReaderSurface),
                    new CameraCaptureSession.StateCallback() {
                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession session) {
                            if( null == cDevice){
                                return;
                            }
                            Log.d(TAG, "Configuration finished");
                            cCaptureSession = session ;
                            try{

                                previewRequest = previewRequestBuilder.build();
                                cCaptureSession.setRepeatingRequest( previewRequest , cCaptureCallback  , cBackgroundHandler );
                                Log.d(TAG, "cCaptureSession.setRepeatingRequest");
                            }catch( CameraAccessException cae ){
                                cae.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(@NonNull CameraCaptureSession session) {
                            Log.d(TAG, "Configuration Failed");
                        }
                    }, null);
        }catch (CameraAccessException cae){
            cae.printStackTrace();
        }
    }

    public void takePicture(){
        try{
                previewRequestBuilder.set(CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER , CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER_START );
                file = getOutputMediaFile();
                previewRequest = previewRequestBuilder.build();
               cCaptureSession.setRepeatingRequest( previewRequest , cCaptureCallback  , cBackgroundHandler );
                captureConfiguration();

        }catch (CameraAccessException cae){
            cae.printStackTrace();
        }

        Toast.makeText(activity.getApplicationContext(), file.getAbsolutePath(), Toast.LENGTH_SHORT).show();
    }

    private void captureConfiguration(){
        try{
            if(null == cDevice){
                return;
            }
            final CaptureRequest.Builder captureBuilder = cDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            captureBuilder.addTarget( imageReader.getSurface());

            captureBuilder.set(CaptureRequest.CONTROL_AE_MODE ,  CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
            setAutoFlash(captureBuilder);

            //int rotation = textureView.getDisplay().getRotation();
            //captureBuilder.set(CaptureRequest.JPEG_ORIENTATION , getOri)

            CameraCaptureSession.CaptureCallback captureCallback = new CameraCaptureSession.CaptureCallback() {
                @Override
                public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
                    //super.onCaptureCompleted(session, request, result);
                    previewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER , CameraMetadata.CONTROL_AF_TRIGGER_CANCEL);
                    setAutoFlash(previewRequestBuilder);

                    try {
                        previewRequest = previewRequestBuilder.build();
                        cCaptureSession.capture( previewRequest , cCaptureCallback , cBackgroundHandler);
                        cCaptureSession.setRepeatingRequest( previewRequest , cCaptureCallback  , cBackgroundHandler );
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }

                }
            };
            cCaptureSession.stopRepeating();
            cCaptureSession.capture(captureBuilder.build() , captureCallback , null );

        }catch(CameraAccessException cae){
            cae.printStackTrace();
        }
    }
    private void startBackgroundThread() {
        cBackgroundThread = new HandlerThread("CameraBackground");
        cBackgroundThread.start();
        cBackgroundHandler = new Handler(cBackgroundThread.getLooper());
    }

    private void stopBackgroundThread() {
        cBackgroundThread.quitSafely();
        try {
            cBackgroundThread.join();
            cBackgroundThread = null;
            cBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void setAutoFlash(CaptureRequest.Builder requestBuilder) {
        if (isFlashAvilable) {
            requestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH);
        }
    }

    private File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Nehemiah");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + "Nehe_" + timeStamp + ".jpg");

        return mediaFile;
    }

    private static class ImageSaver implements Runnable {

        private final Image mImage;
        private final File mFile;

        public ImageSaver(Image image, File file) {
            mImage = image;
            mFile = file;
        }

        @Override
        public void run() {
            ByteBuffer buffer = mImage.getPlanes()[0].getBuffer();
            byte[] bytes = new byte[buffer.remaining()];
            buffer.get(bytes);
            FileOutputStream output = null;
            try {
                output = new FileOutputStream(mFile);
                output.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                mImage.close();
                if (null != output) {
                    try {
                        output.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }
        }
    }

    static class CompareSizesByArea implements Comparator<Size> {

        @Override
        public int compare(Size lhs, Size rhs) {
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() - (long) rhs.getWidth() * rhs.getHeight());
        }

    }
}
